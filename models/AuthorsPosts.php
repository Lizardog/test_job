<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors_posts".
 *
 * @property int $authors_id
 * @property int $posts_id
 *
 * @property Authors $authors
 * @property Posts $posts
 */
class AuthorsPosts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors_posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['authors_id', 'posts_id'], 'required'],
            [['authors_id', 'posts_id'], 'integer'],
            [['posts_id'], 'unique', 'targetAttribute' => ['posts_id']],
            [['authors_id'], 'exist', 'skipOnError' => true, 'targetClass' => Authors::className(), 'targetAttribute' => ['authors_id' => 'id']],
            [['posts_id'], 'exist', 'skipOnError' => true, 'targetClass' => Posts::className(), 'targetAttribute' => ['posts_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'authors_id' => 'Authors ID',
            'posts_id' => 'Posts ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthors()
    {
        return $this->hasOne(Authors::className(), ['id' => 'authors_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasOne(Posts::className(), ['id' => 'posts_id']);
    }
}
