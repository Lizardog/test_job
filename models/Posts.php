<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property string $post_name
 * @property string $post_created
 *
 * @property AuthorsPosts[] $authorsPosts
 * @property Authors[] $authors
 * @property CategoriesPosts[] $categoriesPosts
 * @property Categories[] $categories
 */
class Posts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_created'], 'safe'],
            [['post_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_name' => 'Post Name',
            'post_created' => 'Post Created',
        ];
    }

    public function getAuthorsPosts()
    {
        return $this->hasMany(AuthorsPosts::className(), ['posts_id' => 'id']);
    }


    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'authors_id'])->viaTable('authors_posts', ['posts_id' => 'id']);
    }


    public function getCategoriesPosts()
    {
        return $this->hasMany(CategoriesPosts::className(), ['posts_id' => 'id']);
    }


    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'categories_id'])->viaTable('categories_posts', ['posts_id' => 'id']);
    }
}
