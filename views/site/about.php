<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php foreach ($posts as $num => $post) : ?>
        <div>
            <span><?= $post->id ?> </span>
            <span><?= $post->post_name ?></span><br>
            <span>Категория:<i><?=$post->category['category_name']?></i></span>
            <br>
            <span>Автор:<b><?=$post->author['author_name']?></b></span><br>
            <hr>
        </div>

    <?php endforeach; ?>
    <?= LinkPager::widget([
        'pagination' => $pages,
    ]); ?>
</div>
