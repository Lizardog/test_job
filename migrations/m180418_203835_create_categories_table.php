<?php

use yii\db\Migration;

/**
 * Handles the creation of table `categories`.
 */
class m180418_203835_create_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'category_name' => $this->string(255)->defaultValue('Unsigned'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('categories');
    }
}
