<?php

use yii\db\Migration;

/**
 * Handles the creation of table `authors`.
 */
class m180418_203812_create_authors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('authors', [
            'id' => $this->primaryKey(),
            'author_name' => $this->string(255)->defaultValue('Unsigned'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('authors');
    }
}
