<?php

use yii\db\Migration;

/**
 * Handles the creation of table `authors_posts`.
 * Has foreign keys to the tables:
 *
 * - `authors`
 * - `posts`
 */
class m180418_211828_create_junction_table_for_authors_and_posts_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('authors_posts', [
            'authors_id' => $this->integer(),
            'posts_id' => $this->integer(),
            'PRIMARY KEY(authors_id, posts_id)',
        ]);

        // creates index for column `authors_id`
        $this->createIndex(
            'idx-authors_posts-authors_id',
            'authors_posts',
            'authors_id'
        );

        // add foreign key for table `authors`
        $this->addForeignKey(
            'fk-authors_posts-authors_id',
            'authors_posts',
            'authors_id',
            'authors',
            'id',
            'CASCADE'
        );

        // creates index for column `posts_id`
        $this->createIndex(
            'idx-authors_posts-posts_id',
            'authors_posts',
            'posts_id'
        );

        // add foreign key for table `posts`
        $this->addForeignKey(
            'fk-authors_posts-posts_id',
            'authors_posts',
            'posts_id',
            'posts',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `authors`
        $this->dropForeignKey(
            'fk-authors_posts-authors_id',
            'authors_posts'
        );

        // drops index for column `authors_id`
        $this->dropIndex(
            'idx-authors_posts-authors_id',
            'authors_posts'
        );

        // drops foreign key for table `posts`
        $this->dropForeignKey(
            'fk-authors_posts-posts_id',
            'authors_posts'
        );

        // drops index for column `posts_id`
        $this->dropIndex(
            'idx-authors_posts-posts_id',
            'authors_posts'
        );

        $this->dropTable('authors_posts');
    }
}
