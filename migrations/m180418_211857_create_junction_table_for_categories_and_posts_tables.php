<?php

use yii\db\Migration;

/**
 * Handles the creation of table `categories_posts`.
 * Has foreign keys to the tables:
 *
 * - `categories`
 * - `posts`
 */
class m180418_211857_create_junction_table_for_categories_and_posts_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('categories_posts', [
            'categories_id' => $this->integer(),
            'posts_id' => $this->integer(),
            'PRIMARY KEY(categories_id, posts_id)',
        ]);

        // creates index for column `categories_id`
        $this->createIndex(
            'idx-categories_posts-categories_id',
            'categories_posts',
            'categories_id'
        );

        // add foreign key for table `categories`
        $this->addForeignKey(
            'fk-categories_posts-categories_id',
            'categories_posts',
            'categories_id',
            'categories',
            'id',
            'CASCADE'
        );

        // creates index for column `posts_id`
        $this->createIndex(
            'idx-categories_posts-posts_id',
            'categories_posts',
            'posts_id'
        );

        // add foreign key for table `posts`
        $this->addForeignKey(
            'fk-categories_posts-posts_id',
            'categories_posts',
            'posts_id',
            'posts',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `categories`
        $this->dropForeignKey(
            'fk-categories_posts-categories_id',
            'categories_posts'
        );

        // drops index for column `categories_id`
        $this->dropIndex(
            'idx-categories_posts-categories_id',
            'categories_posts'
        );

        // drops foreign key for table `posts`
        $this->dropForeignKey(
            'fk-categories_posts-posts_id',
            'categories_posts'
        );

        // drops index for column `posts_id`
        $this->dropIndex(
            'idx-categories_posts-posts_id',
            'categories_posts'
        );

        $this->dropTable('categories_posts');
    }
}
