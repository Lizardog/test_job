<?php

use yii\db\Migration;

/**
 * Handles the creation of table `posts`.
 */
class m180418_203820_create_posts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('posts', [
            'id' => $this->primaryKey(),
            'post_name' => $this->string(255)->defaultValue('Example post'),
            'post_created' => $this->timestamp()->defaultValue(null)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('posts');
    }
}
